#!/bin/bash

# 
# Shell script to launch Docker container running the inventory monitor Python3 application.
# 
# This script needs to be pointed to a tokens.json file with the API access tokens (read-only)
# placed into it. It also needs to be pointed to a YAML config file which has serial and IP address
# information on the IPMCs that this application is going to poll.
# 
# Run the script with three arguments:
# 
# 1. Path to the tokens.json file in the current working directory
# 2. Path to the YAML config file in the current working directory
# 3. Docker image tag to run (default is "latest")
# s
# ./start_monitor.sh /path/to/tokens.json /path/to/ipmcs.yaml <imageID>
#

# Check that there are at least two arguments passed to this script
if [ "$#" -lt 2 ];
then
    echo "> At least two arguments must be given to this script."
    echo "> Usage: ./start_monitor.sh /path/to/tokens.json /path/to/ipmcs.yaml <imageTag>"
    echo "> Exiting."
    exit 1
fi

# Absolute paths to tokens.json and YAML config files
TOKEN_FILE_PATH=$(realpath ${1})
CONFIG_FILE_PATH=$(realpath ${2})

# Check if the input paths are valid
if [ ! -f ${TOKEN_FILE_PATH} ]; 
then
    echo "> File does not exist: ${TOKEN_FILE_PATH}, exiting."
    exit 1
fi

if [ ! -f ${CONFIG_FILE_PATH} ]; 
then
    echo "> File does not exist: ${CONFIG_FILE_PATH}, exiting."
    exit 1
fi

# Target mount paths for the tokens.json and YAML config files 
# on the container, they will be mounted under path /app
TOKEN_MOUNT_PATH="/app/tokens.json"
CONFIG_MOUNT_PATH="/app/config.yaml"

# ID of the Docker image to run
IMAGE_ID="alpakpinar/inventory-monitor"

# Append the image tag if it is specified, the default tag is "latest"
if [ ! -z "$3" ];
then
    IMAGE_ID="${IMAGE_ID}:${3}"
fi

# Launch the Docker command
docker run -d \
    --rm \
    --mount type=bind,source=${TOKEN_FILE_PATH},target=${TOKEN_MOUNT_PATH},readonly \
    --mount type=bind,source=${CONFIG_FILE_PATH},target=${CONFIG_MOUNT_PATH},readonly \
    ${IMAGE_ID}