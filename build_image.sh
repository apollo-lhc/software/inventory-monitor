#!/bin/bash

# 
# Shell script to build a Docker image and upload it to Docker Hub.
#
# Currently, all Docker images for this application are stored here:
# https://hub.docker.com/repository/docker/alpakpinar/inventory-monitor
#
# The image tag will be in the following format: 
# alpakpinar/inventory-monitor:<BRANCH>-<HASH>
#
# Note that a specific image tag can be provided from command line as well:
# ./build_image.sh <image_tag>
# 

GIT_BRANCH=$(git branch --show-current)
# Replace slashes in branch name with dashes
GIT_BRANCH=${GIT_BRANCH//\//-}

GIT_HASH=$(git rev-parse --short HEAD)

IMAGE_TAG_BASE="alpakpinar/inventory-monitor"

IMAGE_TAG="${IMAGE_TAG_BASE}:${GIT_BRANCH}-${GIT_HASH}"

# If an image tag is provided from command line, override
if [ ! -z "${1}" ];
then
    IMAGE_TAG="${IMAGE_TAG_BASE}:${1}"
fi

echo "> Building Docker image with tag: ${IMAGE_TAG}"

# Do the build
docker build --tag ${IMAGE_TAG} .

# Push the image
echo "> Pushing the image to Docker Hub"
docker push ${IMAGE_TAG}

echo "> Done"