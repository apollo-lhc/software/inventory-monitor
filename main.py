#!/usr/bin/env python3

import os
import sys
import time
import yaml
import socket
import signal

from threading import Event

from tabulate import tabulate

from util.ipmc import get_ipmcs_to_check_from_config, retrieve_ipmc_objects_from_db

from helpers.graphite import GraphiteEntry, GraphiteHelper
from helpers.telnet import write_command_and_read_output
from helpers.gitlab_utils import retrieve_latest_tag_from_gitlab
from helpers.rest import retrieve_tokens

# Add the current working directory to Python path to be able 
# to do easy-imports from the submodule
CURRENT_DIR = os.path.abspath(os.getcwd())
sys.path.append(CURRENT_DIR)

# YAML configuration file with the IPMCs we want to run on
CONFIG_PATH = "config.yaml"

# Path to the JSON file with the API tokens for SnipeIT and GitLab
TOKEN_PATH = "tokens.json"

# Port to be used for the telnet interface
TELNET_PORT = 23
# Timeout for the telnet socket connection in seconds
TELNET_TIMEOUT = 1

# Default error messages to display in the IPMC table
OS_ERROR_DEFAULT_MSG = "IP Not Found"
TIMEOUT_DEFAULT_MSG  = "Timeout"

# Column names for the IPMC table
IPMC_SERIAL_COLNAME = "IPMC"
ASSIGNED_SM_DB_COLNAME = "SM (DB)"
ASSIGNED_SM_EEPROM_COLNAME = "SM (EEPROM)"
EEPROM_REV_DB_COLNAME = "EEPROM Rev (DB)"
EEPROM_REV_EEPROM_COLNAME = "EEPROM Rev (EEPROM)"
FW_COMMIT_HASH_DB_COLNAME = "FW (DB)"
FW_COMMIT_HASH_EEPROM_COLNAME = "FW (EEPROM)"
MATCH_COLNAME = "Match?"
FW_UP_TO_DATE_COLNAME = "FW Up To Date?"

# If the internal flag of this Event object is set to true
# (by SIGTERM handler), the application stops gracefully.
EXIT_EVENT = Event()

def print_summary_table(ipmc_data: list) -> None:
    """
    Prints a neat summary table of the IPMC data we polled.
    """
    assert len(ipmc_data) > 0, "IPMC data is empty, nothing to print."

    headers = ipmc_data[0].keys()
    rows = [x.values() for x in ipmc_data]
    
    print('\n> IPMC data summary:\n')
    
    print(tabulate(rows, headers, tablefmt='grid'))


def run_app(config: dict):
    """
    Function to run the monitoring application for a single iteration. 
    """
    print(f'Execution start: {time.ctime()}')

    # Retrieve the API access tokens
    try:
        tokens = retrieve_tokens(TOKEN_PATH)
    # Handle the case where we fail to parse the JSON token file
    # or there is no JSON file found under TOKEN_PATH.
    except (RuntimeError, FileNotFoundError) as e:
        print(f"{str(e)}, exiting.")
        return

    # Figure out which IPMCs we want to check from the configuration file
    # Note that this is a dictionary mapping the following: 
    # IPMC serial number -> IPMC IP address
    try:
        ipmcs_to_check = get_ipmcs_to_check_from_config(config)
    
    # Catch the RuntimeError which occurs when the YAML config file 
    # does not have the top-level key "ipmcs".
    except RuntimeError as e:
        print(f"{str(e)}, exiting.")
        return

    # Make the GET request to the SnipeIT REST API and retrieve the HW data
    print(f'Retrieving HW data from Snipe IT DB')
    db_start = time.time()
    
    # Retrieve SnipeIT related config information
    try:
        snipeit_settings = config["snipeit"]
    except KeyError:
        print("SnipeIT settings are not provided in config (under 'snipeit' field), exiting.")
        return

    ipmcs = retrieve_ipmc_objects_from_db(
        base_url=snipeit_settings["base_url"],
        token=tokens["snipeit"],
        serials_to_check=ipmcs_to_check.keys()
    )
    
    db_finish = time.time()

    # Check if the items were retrieved without error from the DB
    if not ipmcs:
        print(f'Error encountered in HTTP response, exiting.')
        return

    print(f'HTTP transaction complete ({db_finish - db_start:.2f} sec)')

    # 
    # GitLab API
    # Retrieve information for the latest FW tag from GitLab API
    # 
    try:
        gitlab_settings = config["gitlab"]
    except KeyError:
        print("GitLab API settings are not provided in config (under 'gitlab'), exiting.")
        return
    
    try:
        latest_tag_info = retrieve_latest_tag_from_gitlab(gitlab_settings["project_id"], tokens["gitlab"])

    # Handle the case where we don't get a valid response
    except RuntimeError as e:
        print(f'Got non-OK response from GitLab API: {str(e)}, exiting.')
        return

    print(f"GitLab API: Latest stable tag retrieved")
    print(f"Tag: {latest_tag_info['tag']}")
    print(f"Commit: 0x{latest_tag_info['commit']}")

    # 
    # Graphite-related setup. This helper object will be used 
    # to push data to Graphite server.
    # 
    try:
        graphite_helper = GraphiteHelper(config)
        # Data for Graphite server
        graphite_entries = []

    # Handle the case if the YAML config is not valid for Graphite
    except RuntimeError as e:
        print(f'{str(e)}, exiting.')
        return

    # IPMC data array is used for the final table we're going to print
    ipmc_data = []

    # Number of items where the DB and EEPROM based information matches 
    matching_count = 0

    # Number of items where the FW commit matches the latest tag
    fw_up_to_date_count = 0

    # Loop over the IPMC items and for each, find out which SM they're assinged to
    # according to the SnipeIT DB and the IPMC's EEPROM.
    for ipmc in ipmcs:
        # Extract custom field information on this IPMC from the DB
        serial       = ipmc.serial
        revision_db  = ipmc.db_info["IPMC EEPROM Revision"]
        commit_db    = ipmc.db_info["IPMC FW Commit Hash"]
        sm_serial_db = ipmc.db_info["Assigned SM Serial"]

        # Create a socket connection to this IPMC to poll information
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            # Retrieve the IP address for this IPMC from the .txt config file
            HOST = ipmcs_to_check[serial]

            # Graphite entry for this IPMC
            graphite_entry = GraphiteEntry(serial)

            # Establish connection
            try:
                s.connect((HOST, TELNET_PORT))
                s.settimeout(TELNET_TIMEOUT)
            
            # Handle the case where the IP address of the IPMC cannot be found
            except OSError:
                ipmc_data.append({
                    IPMC_SERIAL_COLNAME            : serial,
                    ASSIGNED_SM_DB_COLNAME         : sm_serial_db,
                    ASSIGNED_SM_EEPROM_COLNAME     : OS_ERROR_DEFAULT_MSG,
                    EEPROM_REV_DB_COLNAME          : revision_db,
                    EEPROM_REV_EEPROM_COLNAME      : OS_ERROR_DEFAULT_MSG,
                    FW_COMMIT_HASH_DB_COLNAME      : commit_db,
                    FW_COMMIT_HASH_EEPROM_COLNAME  : OS_ERROR_DEFAULT_MSG,
                    MATCH_COLNAME                  : "False",
                    FW_UP_TO_DATE_COLNAME          : OS_ERROR_DEFAULT_MSG,
                })

                graphite_entry.set_attribute("DB_MATCH", 0)
                graphite_entry.set_attribute("FW_UP_TO_DATE", 0)
                graphite_entries.append(graphite_entry)
                continue             

            # 
            # Execute the commands on the telnet interface of the IPMC to 
            # retrieve information.
            # 
            try:
                # Execute the eepromrd and info commands and catch the output
                stdouts = {}
                commands = ['eepromrd', 'info']
                for command in commands:
                    stdouts[command] = write_command_and_read_output(s, f'{command}\r\n')

                # Parse command outputs and retrieve information from IPMC
                ipmc.get_from_telnet(stdouts)
                
                sm_serial_eeprom = ipmc.telnet_info["Assigned SM Serial"]
                revision_eeprom  = ipmc.telnet_info["IPMC EEPROM Revision"]
                commit_eeprom    = ipmc.telnet_info["IPMC FW Commit Hash"]
                
                # Figure out if the records in DB and EEPROM match
                match = (sm_serial_db == sm_serial_eeprom) \
                    and (revision_db == revision_eeprom) \
                    and (commit_db == commit_eeprom)
                
                # Check if FW is up to date with the latest tag
                fw_up_to_date = latest_tag_info["commit"] == commit_eeprom.replace('0x','')
                if fw_up_to_date:
                    fw_up_to_date_label = f"{latest_tag_info['tag']} installed"
                    fw_up_to_date_count += 1
                else:
                    fw_up_to_date_label = f"{latest_tag_info['tag']} not installed"

                # Add this record to the output table
                ipmc_data.append({
                    IPMC_SERIAL_COLNAME            : serial,
                    ASSIGNED_SM_DB_COLNAME         : sm_serial_db,
                    ASSIGNED_SM_EEPROM_COLNAME     : sm_serial_eeprom,
                    EEPROM_REV_DB_COLNAME          : revision_db,
                    EEPROM_REV_EEPROM_COLNAME      : revision_eeprom,
                    FW_COMMIT_HASH_DB_COLNAME      : commit_db,
                    FW_COMMIT_HASH_EEPROM_COLNAME  : commit_eeprom,
                    MATCH_COLNAME                  : match,
                    FW_UP_TO_DATE_COLNAME          : fw_up_to_date_label,
                })
                
                # If this is a match, increment the number of matching records
                if match:
                    matching_count += 1
            
                # Set Graphite attributes and add this IPMC to the list of Graphite entries
                graphite_entry.set_attribute("DB_MATCH", int(match))
                graphite_entry.set_attribute("FW_UP_TO_DATE", int(fw_up_to_date))
                graphite_entries.append(graphite_entry)

            # Handle the event of a socket timeout
            except socket.timeout:
                ipmc_data.append({
                    IPMC_SERIAL_COLNAME            : serial,
                    ASSIGNED_SM_DB_COLNAME         : sm_serial_db,
                    ASSIGNED_SM_EEPROM_COLNAME     : TIMEOUT_DEFAULT_MSG,
                    EEPROM_REV_DB_COLNAME          : revision_db,
                    EEPROM_REV_EEPROM_COLNAME      : TIMEOUT_DEFAULT_MSG,
                    FW_COMMIT_HASH_DB_COLNAME      : commit_db,
                    FW_COMMIT_HASH_EEPROM_COLNAME  : TIMEOUT_DEFAULT_MSG,
                    MATCH_COLNAME                  : "False",
                    FW_UP_TO_DATE_COLNAME          : TIMEOUT_DEFAULT_MSG,
                })
                graphite_entry.set_attribute("DB_MATCH", 0)
                graphite_entry.set_attribute("FW_UP_TO_DATE", 0)
                graphite_entries.append(graphite_entry)
                
    # Print out a summary table for the IPMCs we polled
    # print_summary_table(ipmc_data)

    print(f'{matching_count}/{len(ipmc_data)} IPMC records match between DB and EEPROM.')
    print(f'{fw_up_to_date_count}/{len(ipmc_data)} IPMC records have the latest FW.')

    # Push data to graphite server
    if graphite_helper.do_push:
        print(f'Pushing data to Graphite server: {time.ctime()}')
        graphite_helper.push_to_graphite(graphite_entries)
    
    print(f'Done: {time.ctime()}')


def main():
    """
    Main entrypoint for this application.
    """
    # Check if the YAML config file is there
    if not os.path.exists(CONFIG_PATH):
        print(f'Cannot find configuration file: {CONFIG_PATH}, exiting.')
        return

    # Obtain the sleeping time from config, default is 1 hour
    with open(CONFIG_PATH, 'r') as f:
        config = yaml.safe_load(f)

    default_sleep_time = 3600
    sleep_time = config.get("poll_time", default_sleep_time)

    def signal_handler(signum, frame):
        """
        Signal handler to deal with SIGTERM signal coming from docker stop.
        """
        print(f'Interrupted by signal {signum}, shutting down.')

        # Sets the internal flag of EXIT_EVENT to True, so that
        # we stop at the next iteration
        EXIT_EVENT.set()

    # Register the signal handler
    signal.signal(signal.SIGTERM, signal_handler)

    # 
    # Main loop: Run the application and sleep.
    # Continue looping until the process is killed by a SIGTERM signal.
    # If a SIGTERM is received (by docker stop), shut down gracefully.
    # 
    while not EXIT_EVENT.is_set():
        run_app(config)
        
        print(f'Sleeping for: {sleep_time} s.')
        EXIT_EVENT.wait(sleep_time)


if __name__ == '__main__':
    main()