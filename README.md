# Inventory Monitor

This repository contains a Python3 application that is used to validate the IPMC information in the Snipe IT database. It uses the REST API interface (docs [here](https://snipe-it.readme.io/reference/api-overview)) of the Snipe IT database to retrieve IPMC-related information, and checks it against information read out from IPMCs via their `telnet` interface.

## Running The Code

The application can be run as a Docker container. Currently, the Docker images are hosted under Docker Hub, [here](https://hub.docker.com/repository/docker/alpakpinar/inventory-monitor). There is a shell script, `start_monitor.sh`, which should be run to start the Docker container interactively.

**Note:** Prior to launching the application, a `tokens.json` file having the API access tokens for the SnipeIT and GitLab APIs should be requested. 

`start_monitor.sh` script should be pointed to two files as arguments:

- Path to `tokens.json` file, which has the API keys for SnipeIT and GitLab APIs, this will be mounted under `/app/tokens.json` in the container.
- Path to a YAML configuration file, which has the list of IPMCs for the application to poll, and (optionally) DNS information to resolve IPMC host names. This will be mounted under the path `/app/config.yaml`.

For details on the YAML configuration file structure, please see the "Configuring the Application" section below.

The final (third) argument to `start_monitor.sh` should be the image tag to be ran (name of the image following `alpakpinar/inventory-monitor`). Check out [here](https://hub.docker.com/repository/docker/alpakpinar/inventory-monitor) to see the full list of image versions available for pulling.

With those three arguments, `start_monitor.sh` script can be run as follows:

```bash
./start_monitor.sh /path/to/tokens.json /path/to/config.yaml ${IMAGE_TAG}
```

## Configuring the Application

The application can be configured for custom use cases, using a YAML configuration file. An example file can be found under `example/BU_config.yaml`.

The user can configure the following pieces of information about the application:

* List of IPMCs to look at and their expected IP addresses (or host names)
* DNS configuration
* Graphite/Grafana related configuration

### List of IPMCs

The list of IPMCs and their expected IP addresses must be listed under a YAML configuration file. The application will poll information **only** for the IPMCs listed here, and will use the IP address given to contact each IPMC via the `telnet` interface.

If a valid DNS configuration is given in the same file, host names for IPMCs can also be used. Please see the "DNS Configuration" sub-section below to see how this can be done.

The YAML configuration file should contain IPMC data under the `ipmcs` top-level entry, in the following format:

```yaml
# ipmcs top-level key must exist, otherwise the config file is not valid!
ipmcs:
    serial_num_1: <ip_addr_1 or hostname_1>
    serial_num_2: <ip_addr_2 or hostname_2>
    ...
```

### DNS Configuration

If host names are provided for IPMCs in the YAML configuration file, a valid DNS configuration **must** be provided so that it can be used to resolve those host names, and retrieve the IP addresses.

The YAML configuration file should have the DNS information as follows:

```yaml
dns:
  server:
    - <IP address of nameserver>
  search_domain:
    - <List of domains to search>
```

### Graphite Configuration

Inventory monitoring software also has the ability to push per-IPMC data into a specified Graphite server. This behavior can be configured in the YAML configuration file as follows:

```yaml
graphite:
  # Whether we want to push to Graphite server 
  push: true 
  # First two fields of the Graphite name
  identifier: "INVENTORY.IPMC"
  # Info on the Graphite server
  server:
    ip: <ip_address>
    port: <port>
```

For every IPMC specified in the configuration, two pieces of information will be pushed to the Graphite server:

* `INVENTORY.IPMC.{serial}.DB_MATCH (0/1)`: Checks if DB and EEPROM info matches.
* `INVENTORY.IPMC.{serial}.FW_UP_TO_DATE (0/1)`: Checks if the FW installed is the latest stable tag retrieved from GitLab API.

Each field will be `1` if the checking condition evaluates to `true`, otherwise it will be set `0`. Note that the IPMC serials displayed will be in hex format. Note that the first two parts of the Graphite identifier ("INVENTORY.IPMC" in this example) is determined by the `identifier` field in the YAML configuration.

Note that the Graphite pushing can be disabled by the user by setting `push: false` in the YAML configuration file:

```yaml
# Configuration for no pushing to Graphite
graphite:
  push: false
  # These entries will be ignored now
  identifier: "INVENTORY.IPMC"
  server:
    ip: <ip_address>
    port: <port>
```

### SnipeIT & GitLab Customizations

SnipeIT and GitLab REST API related configurations are listed under `snipeit` and `gitlab` fields, respectively. Under the `snipeit` field, user can modify the URL to which the `GET` request will be sent. Under the `gitlab` field, user can modify the `project_id` field, which specifies the project ID for the OpenIPMC FW project on GitLab.

An example configuration for this is as follows:

```yaml
snipeit:
  base_url: 'http://ohm.bu.edu:8000/api/v1/hardware' # Endpoint for API calls
gitlab:
  project_id: 25395488 # Project ID for the OpenIPMC FW on GitLab
```

### Polling Frequency

Once run, the Docker image will poll the IPMCs and the SnipeIT database continuously. The polling frequency (in seconds) can be set in the YAML configuration file. 

For example, the following configuration will make the application do the polling every hour:

```yaml
poll_time: 3600
```


## Building a Docker Image

To build a Docker image with the current codebase, user can simply run the `build_image.sh` shell script:

```bash
./build_image.sh
```

This will build a Docker image and push it to Docker hub [here](https://hub.docker.com/repository/docker/alpakpinar/inventory-monitor), 
with the following image tag:

```bash
IMAGE_TAG=${GIT_BRANCH_NAME}-${GIT_COMMIT_HASH}

# The complete image name specifier on Docker Hub will be:
alpakpinar/inventory-monitor:${IMAGE_TAG}
```

Note that a specific image tag can be provided from command line as well:

```bash
./build_image.sh ${IMAGE_TAG}
```