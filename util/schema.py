# 
# Schema definitions for hardware.
# 

import re

from dataclasses import dataclass
from typing import Dict


@dataclass
class ApolloSM:
    serial: str
    db_id: str # Database ID for this SM

    def __str__(self) -> str:
        """
        Stringified version of this object is it's serial number.
        """
        return self.serial


@dataclass
class IPMC:
    """
    Class representing a single IPMC with a serial 
    (must be given during initialization).

    This class has db_info and telnet_info fields, which specify
    the info gathered from Snipe IT DB, and TelNet CLI respectively.

    Each of these two fields will be a dict with the following format:
    {
        "IPMC FW Commit Hash"     : "XXX" (str),
        "IPMC EEPROM Revision"    : "XXX" (str),
        "Assigned SM Serial"      : "XXX" (str),
    }
    """
    serial: str

    def __post_init__(self):
        KEYS = [
            "IPMC FW Commit Hash",
            "IPMC EEPROM Revision",
            "Assigned SM Serial",
        ]
        
        # Objects representing info from DB and TelNet CLI.
        # 
        # Initially, they are all None, these will be filled with
        # get_from_db() and get_from_telnet() function calls.
        self.db_info, self.telnet_info = {}, {}

        for key in KEYS:
            self.db_info[key] = None
            self.telnet_info[key] = None

        # DB ID of the assigned Service Module (SM)
        # If no SM is assigned to this IPMC, the field will be None.
        self.assigned_sm_db_id = None


    def get_from_db(self, ipmc_json: dict) -> None:
        """
        Given the JSON item representing this IPMC, read out and fill 
        the necessary fields we're interested in.

        If a value for a field is not found, it will be represented as
        None.
        """
        fields = [
            "IPMC FW Commit Hash",
            "IPMC EEPROM Revision",
        ]

        for field in fields:
            try:
                val = ipmc_json["custom_fields"][field]["value"]
                # For FW hash, add "0x" in the beginning for compatibility
                if field == "IPMC FW Commit Hash" and val != "":
                    val = f"0x{val}"
                self.db_info[field] = val
        
            # Could not locate the field in the DB, the value will be None.
            except KeyError:
                continue

        # Get the database ID of the assigned SM
        assigned_to = ipmc_json["assigned_to"]
        if assigned_to:
            self.assigned_sm_db_id = assigned_to["id"]        


    def get_from_telnet(self, stdouts: Dict[str, str]) -> None:
        """
        Get and fill information from TelNet CLI.
        
        stdouts argument MUST be a dictionary of the following format:
        {
            "eepromrd" : <stdout> (str),
            "info"     : <stdout> (str),
        }

        If a value is not found from stdout, it's value will be 
        filled as None.
        """
        for key, stdout in stdouts.items():
            if key == 'eepromrd':
                self._read_from_eepromrd_stdout(stdout)
            elif key == 'info':
                self._read_from_info_stdout(stdout)


    def _read_from_eepromrd_stdout(self, stdout: str) -> None:
        """Retrieve info from eepromrd command output."""
        for line in stdout.split('\n'):
        # Strip whitespace from the line
            line = line.strip()
            tokens = line.split()
            # Line where the HW information is displayed
            if line.startswith('hw') and '#' in line:
                value = re.findall('#(\d+)', tokens[-1])[0]
                self.telnet_info["Assigned SM Serial"] = value

            # Line where the IPMC EEPROM Revision is displayed
            elif line.startswith('prom version'):
                self.telnet_info["IPMC EEPROM Revision"] = str(int(tokens[-1], base=16))

    
    def _read_from_info_stdout(self, stdout: str) -> None:
        """Retrieve info from info command output."""
        commit = None
        for line in stdout.split('\n'):
            # Strip whitespace from the line
            line = line.strip()
            if not line.startswith('Firmware commit:'):
                continue

            commit = line.split()[-1]
        
        # Prepend with 0x if we found a commit value
        if commit:
            commit = f"0x{commit}"
        
        self.telnet_info["IPMC FW Commit Hash"] = commit
