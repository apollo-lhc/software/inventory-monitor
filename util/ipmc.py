# 
# General-utility functions used by other Python3 scripts are defined here.
# 

import os
import yaml
import socket
import dns.resolver

from typing import Dict, List

from helpers.rest import retrieve_response_from_snipeit_db

from helpers.dns_utils import (
    configure_and_return_dns_server,
    resolve_host_name
)

from util.schema import IPMC, ApolloSM


def get_ipmcs_to_check_from_config(config: dict) -> Dict[str, str]:
    """
    Given the configuration holding which IPMCs to check
    and their expected IP addresses (or their equivalent host names),
    construct and return a dictionary of:

    {
        <IPMC Serial #1>: <IPMC IP #1>,
        <IPMC Serial #2>: <IPMC IP #2>,
        ...
    }

    All data types (including serial numbers) are strings.

    If the configuration file is not found under the given path, raises a
    FileNotFoundError. If the "ipmcs" top-level key is not found under the 
    config file, raises a KeyError.
    """
    # Map with the IPMC serial -> IPMC IP address mapping
    ipmc_map = {}
    
    # Set up DNS server for host name resolving
    # If a DNS server config is not specified in the YAML config file,
    # this function will return None.
    dns_server = configure_and_return_dns_server(config)

    # Get the "ipmcs" key, if this key does not exist, catch the KeyError
    # from the calling main() function.
    try:
        data = config["ipmcs"]
    except KeyError:
        raise RuntimeError("'ipmcs' top-level key not found under configuration.")

    # Stringify everything (including serials) and return
    for serial, addr in data.items():
        # Check if this is a valid IP address, in that case
        # just add the entry to the IPMC map and continue.
        try:
            socket.inet_aton(addr)
            ipmc_map[str(serial)] = addr
        
        # Try to resolve this host name since it's not a valid IP
        # If a DNS server configuration is not given, we cannot
        # resolve it and we'll skip this entry.
        except OSError:
            if not dns_server:
                print(f'WARNING: Cannot resolve {addr} since DNS server is not provided, skipping.')
                continue
            
            # Try to resolve the host name
            try:
                ip_addr = resolve_host_name(dns_server, addr)
                ipmc_map[str(serial)] = ip_addr
            # Handle the case where we failed to resolve the given host name
            except dns.resolver.NXDOMAIN:
                print(f'WARNING: Failed to resolve {addr}, skipping.')
                continue
        
    return ipmc_map


def retrieve_ipmc_objects_from_db(base_url: str, token: str, serials_to_check: List[str]) -> List[IPMC]:
    """
    Main function that makes a GET request to SnipeIT REST API.
    Returns a list of IPMC instances:

    -> [<IPMC #0>, <IPMC #1>, ...] 

    Returns all the retrieved hardware as a list of JSON items.
    If the HTTP response is an error, returns an empty list.

    Only returns IPMC objects for the serial numbers that are within
    the serials list.
    """
    items = retrieve_response_from_snipeit_db(base_url, token)
    
    ipmcs = []
    # Keep track of ApolloSMs for inner use
    apollos = []
    
    # 
    # First loop: 
    # Loop over JSON items and construct IPMC and ApolloSM objects.
    # 
    for item in items:
        model_name = item["model"]["name"]
        
        # Found an IPMC item
        if model_name == "IPMC":
            serial = item["custom_fields"]["IPMC Serial Number"]["value"]
            
            # If this serial is not one of the serials we want to check,
            # skip it.
            if serial not in serials_to_check:
                continue

            # Create an IPMC object for this entry
            ipmc = IPMC(serial)

            # Fill DB-related entries for this IPMC
            ipmc.get_from_db(item)
            ipmcs.append(ipmc)

        # Found an ApolloSM item
        elif model_name == "Apollo Blade Service Module":
            serial = item["custom_fields"]["Apollo Serial Number"]["value"]
            
            # Create an ApolloSM object for this entry
            apollosm = ApolloSM(serial, db_id=item["id"])
            apollos.append(apollosm)

    #
    # Second loop:
    # Loop over IPMCs to see which ApolloSMs are assigned to them. 
    # 
    for ipmc in ipmcs:
        # If this IPMC is not assigned to any ApolloSM, skip.
        assigned_id = ipmc.assigned_sm_db_id
        if not assigned_id:
            continue
        
        # Find the corresponding ApolloSM object for this DB ID
        for apollo in apollos:
            if apollo.db_id == assigned_id:
                ipmc.db_info["Assigned SM Serial"] = str(apollo)

    return ipmcs
