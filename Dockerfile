FROM python:3.9-slim-buster

WORKDIR /app

COPY requirements.txt requirements.txt

# Install requirements for the Python project
RUN pip3 install -r requirements.txt

# Copy the entire source code to the working directory
COPY . .

# Run the application, use unbuffered output with -u
ENTRYPOINT [ "python3", "-u", "main.py" ]
